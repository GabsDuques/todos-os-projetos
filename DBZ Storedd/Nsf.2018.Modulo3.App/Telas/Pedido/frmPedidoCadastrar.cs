﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Pedido;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {

        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();

        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
            Configurargrid();
        }

        

        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Produto);
            cboProduto.DataSource = lista;

        }


        void Configurargrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ProdutoDTO produto = cboProduto.SelectedItem as ProdutoDTO;

            int qtd = Convert.ToInt32(txtQuantidade.Text);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(produto);
            }
        }



        private void btnEmitir_Click(object sender, EventArgs e)
        {
            PedidoDTO dto = new PedidoDTO();
            dto.Cliente = txtCliente.Text;
            dto.Cpf = txtCpf.Text;
            dto.Data = DateTime.Now;

            PedidoBusiness business = new PedidoBusiness();
            business.Salvar(dto, produtosCarrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }



        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
