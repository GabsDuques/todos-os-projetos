﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO ProdutoDTO)
        {
            string script = @"INSERT INTO tb_produto (vl_preco, nm_produto)
	                               VALUES (@vl_preco, @nm_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("vl_preco", ProdutoDTO.Preco));
            parms.Add(new MySqlParameter("nm_produto", ProdutoDTO.Produto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<ProdutoDTO> Listar()
        {
            string script = @"select * from tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Produto = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
